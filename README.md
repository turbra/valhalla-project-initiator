# GitLab Project Creation Script

A straightforward script to automatically create projects in a specified GitLab group using the GitLab API.

## Prerequisites

1. **jq**: A lightweight and flexible command-line JSON processor. It's used to parse the JSON responses from the GitLab API. Install it using your package manager, e.g., `sudo apt install jq` for Debian/Ubuntu or for RHEL/CentOS/Fedora `sudo dnf install jq`.

2. **curl**: A command-line tool for transferring data with URLs. It's used to make the API requests. Install it using your package manager if you don't have it.

3. **GitLab Personal Access Token**: Generate a personal access token from your GitLab profile settings with the required permissions to create projects.

## How to Use

1. **Set Your Token & Group ID**:
   Open the script and replace `YOUR_PRIVATE_TOKEN` with your personal access token and `YOUR_GROUP_ID` with the ID of the group/subgroup where you want to create the projects.

2. **Add Project Names**:
   Add the names of the projects you want to create to the `project_names.txt` file, one name per line.

3. **Execute the Script**:
   ```bash
   chmod +x your_script_name.sh
   ./your_script_name.sh
   ```

### How to Get Group ID

To fetch the ID of a group or subgroup in GitLab:

```bash
curl --header "PRIVATE-TOKEN: YOUR_PRIVATE_TOKEN" "https://gitlab.com/api/v4/groups?search=YOUR_GROUP_NAME"
```

Replace `YOUR_PRIVATE_TOKEN` with your personal access token and `YOUR_GROUP_NAME` with the name of your group. The returned JSON will contain the ID of the group which you can use in the script.

## Note

Be cautious while running scripts that interact with APIs, especially ones that create or delete resources. Always double-check the configurations and maybe do a test run with a smaller dataset first.

