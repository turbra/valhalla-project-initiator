#!/bin/bash

# Your personal access token from GitLab
TOKEN="REPLACE-ME"

# Base URL of your GitLab instance. For gitlab.com it's as below.
BASE_URL="https:/REPLACE-ME/api/v4/projects/"

# Group ID where the project should be created
GROUP_ID="REPLACE-ME"

# Read the project names from the file
while IFS= read -r project
do
    echo "Creating project: $project in group ID: $GROUP_ID"

    # Create the project under the group using curl with additional parameters to initialize it with a README
    RESPONSE=$(curl --silent --request POST --header "PRIVATE-TOKEN: $TOKEN" \
                    --data "name=$project" \
                    --data "initialize_with_readme=true" \
                    --data "namespace_id=$GROUP_ID" \
                    "$BASE_URL")

    # Check if the project was created successfully by parsing the JSON response for the "name" attribute
    PROJECT_NAME=$(echo "$RESPONSE" | jq -r '.name')

    if [ "$PROJECT_NAME" == "$project" ]; then
        echo "Created project: $project in group ID: $GROUP_ID successfully"
    else
        echo "Failed to create project: $project in group ID: $GROUP_ID"
    fi

    # Optional: Add a sleep between requests to prevent overwhelming the API or hitting rate limits
    sleep 2
done < project_names.txt
